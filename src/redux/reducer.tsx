import {createSlice, PayloadAction} from "@reduxjs/toolkit";
//
export type FormState = {
    stepValues: any
}
const initialState: FormState = {
    stepValues: {}
}

export const formSlice = createSlice({
    name: "movie",
    initialState,
    reducers: {
        setStepValue: (state:FormState, action: PayloadAction<{values :any}>) => {
            state.stepValues = action.payload.values
         },
        }

})
export const { setStepValue } = formSlice.actions

export default formSlice.reducer