import React, {useState} from 'react'
import './Navbar.css';
import {Link} from "react-router-dom";
import { FiCode, FiMenu, FiX } from "react-icons/fi";
function Navbar() {
    const [click, setClick] = useState(false);
    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    return (
        <>
            <nav className='navbar'>
                <div className='navbar-container'>
                    <Link to='/' className='navbar-logo' >
                        NextJS Product
                        <i className='fab fa-typo3' />
                    </Link>
                    <Link to='/' className='navbar-logo' >
                        NextJS Product
                        <i className='fab fa-typo3' />
                    </Link>
                </div>
            </nav>
            <div className="header">
                <div className="container">
                    <div className="header-con">
                        <div className="logo-container">
                            <a href="#">ReDev <FiCode /></a>
                        </div>
                        <ul className={click ? "menu active" : "menu"}>
                            <li className="menu-link" onClick={closeMobileMenu}>
                                <a href="#">ABOUT</a>
                            </li>
                            <li className="menu-link" onClick={closeMobileMenu}>
                                <a href="#">CONTACT</a>
                            </li>
                            <li className="menu-link" onClick={closeMobileMenu}>
                                <a href="#">BLOG</a>
                            </li>
                        </ul>
                        <div className="mobile-menu" onClick={handleClick}>
                            {click ? (
                                <FiX />
                            ) : (
                                <FiMenu />
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Navbar;