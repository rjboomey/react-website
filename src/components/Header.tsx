import React, { useState } from 'react'
import { FiMenu, FiX } from "react-icons/fi";
import './Header.css'
import {NavLink} from "react-router-dom";




function Header() {

    const [click, setClick] = useState(false);
    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);
    return (

        <div className="header">
            <div className="container">
                <div className="header-con">
                    <div className="logo-container">

                    </div>
                    <ul className={click ? "menu active" : "menu"}>
                        <li className="menu-link" onClick={closeMobileMenu}>
                            <nav>
                                <NavLink to="/"> HOME-PRODUCT</NavLink>
                            </nav>
                        </li>
                        <li className="menu-link" onClick={closeMobileMenu}>
                            <nav>
                                <NavLink to="/product">  CREATE-PRODUCT</NavLink>
                            </nav>
                        </li>
                        <li className="menu-link" onClick={closeMobileMenu}>
                            <nav>
                                <NavLink to="/apipoke"> API</NavLink>
                            </nav>
                        </li>
                    </ul>
                    <div className="mobile-menu" onClick={handleClick}>
                        {click ? (
                            <FiX />
                        ) : (
                            <FiMenu />
                        )}
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Header