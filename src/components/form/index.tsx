import * as Yup from 'yup'


export const validatePD = Yup.object().shape({
    product: Yup.string().required('กรุณาระบุุสินค้า'),
    price: Yup.number().required('กรุณาระบุุราคา'),
    expireDate: Yup.string().when('listExpire', {
        is: 'หมดอายุ',
        then: (expireDate) => expireDate.required('กรุณาระบุุวันหมดอายุ')
    }),
})