import React,{useState} from "react";

import {
    TableRow,
    tableCellClasses,
    TableCell,
    styled,
    Link,
    Paper,
    TableContainer,
    TableHead,
    Table,
    TableBody,
    Card,
    CardMedia,
    CardContent,
    Typography,
    CardActions,
    Grid,
} from "@mui/material";
import axios from "axios";
import Button from "@mui/material/Button";



export default function Pokemon() {
    const [data,setData] = useState<any[]>([])
    const [height,setPokemonHeight] = useState()
    const [pokemonName,setPokemonName] = useState()
    const [level,setPokemonLevel] = useState()
    const [weight,setPokemonWeight] = useState()
    const [pic, setPic] = useState()
    const [display, setDisplay] = useState(false)
    const [favouriteItem, setFavouriteItem] = useState<any>()

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));



    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));



    const  pokemokeApi = () => {
        axios.get("https://pokeapi.co/api/v2/pokemon").then(function (res) {
            setData(res.data.results)
        })
    }

    const  pokemokeApi2 = (name: any) => {
        async  function getData() {
            const  res = await axios.get(`https://pokeapi.co/api/v2/pokemon/${name}`)
            setPokemonHeight(res.data.height)
            setPokemonLevel(res.data.base_experience)
            setPokemonWeight(res.data.weight)
            setPokemonName(res.data.name.toUpperCase())
            setPic(res.data.sprites.front_default)

        }
        setDisplay(true)
        getData();
    }

    const  local = () => {
        localStorage.setItem('pokemon',JSON.stringify(pokemonName))
        setFavouriteItem(JSON.parse(localStorage.getItem('pokemon') as string))
    }

    const  deletelocal = () => {
        localStorage.removeItem("pokemon")
        setFavouriteItem(JSON.parse(localStorage.getItem('pokemon') as string))
    }

    React.useEffect(() => {
        pokemokeApi()

    }, [])

    return (
        <>
            <form className={'form'}>
                <Grid container spacing={2} columns={1} alignItems="center">
                    {!display &&(
                        <>
                <TableContainer component={Paper} >
                    <Table sx={{ minWidth: 150 } } aria-label="customized table" >
                        <TableHead>
                            <TableRow>
                                <StyledTableCell>Pokemon</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.map((row,index) => (
                                <StyledTableRow key={row.name}>
                                    <StyledTableCell component="th" scope="row" >
                                        <Link
                                            component="button"
                                            type={'button'}
                                            underline="always"
                                            onClick={() => {
                                                pokemokeApi2(row.name)
                                            }}
                                        >
                                            {row.name}
                                        </Link>
                                    </StyledTableCell>
                                </StyledTableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                </>
                        )}
                </Grid>
                {display &&(
                    <>
                <Card sx={{ maxWidth: 500,background: "#FAF0E6" }} className={'card-poke'}>
                    <CardMedia
                        sx={{ height: 250 }}
                        image={pic}
                        title="green iguana"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            {pokemonName}
                        </Typography>
                        {favouriteItem === pokemonName &&(
                            <>
                        <Typography gutterBottom variant="h5" component="div">
                            <h6 style={{ color: 'red' }}>Level:  <span style={{ color: 'blue' }}>{level}</span></h6>
                        </Typography>
                        <Typography gutterBottom variant="h5" component="div">
                            <h6 style={{ color: 'red' }}>Height:  <span style={{ color: 'blue' }}>{height}</span></h6>
                        </Typography>
                        <Typography gutterBottom variant="h5" component="div">
                            <h6 style={{ color: 'red' }}>Weight:  <span style={{ color: 'blue' }}>{weight}</span></h6>
                        </Typography>
                        </>
                            )}

                    </CardContent>
                    <CardActions>
                        <Button size="small" onClick={() => local()}>ADD Favourite</Button>
                    </CardActions>
                    <CardActions>
                        <Button size="small" onClick={() => deletelocal()}>Delete Favourite</Button>
                    </CardActions>
                </Card>
                <Grid container spacing={2} columns={1} alignItems="center" className={'back'}>
                    <Button variant="contained" onClick={() =>
                        setDisplay(false)
                    }>
                    Back
                    </Button>
                </Grid>
                    </>
                )}
            </form>
        </>
    )

}


