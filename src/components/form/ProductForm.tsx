import {Autocomplete, Box, Grid, TextField, Typography} from "@mui/material";
import * as React from 'react';
import FormLabel from "@mui/material/FormLabel";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";

import {useSelector, useDispatch} from 'react-redux'
import Button from "@mui/material/Button";
import {formSelector} from "../../redux/selector";
import {setStepValue} from "../../redux/reducer"
import {useFormik} from "formik";
import {validatePD} from "./index";


export default function Product() {

    const dateNow = new Date();
    const year = dateNow.getFullYear();
    const monthWithOffset = dateNow.getUTCMonth() + 1;
    const month =
        monthWithOffset.toString().length < 2
            ? `0${monthWithOffset}`
            : monthWithOffset;
    const date =
        dateNow.getUTCDate().toString().length < 2
            ? `0${dateNow.getUTCDate()}`
            : dateNow.getUTCDate();
    const materialDateInput = `${year}-${month}-${date}`;
    const options = ['ของใช้ในบ้าน', 'ของใช้นอกบ้าน'];

    const [productType, setProductType] = React.useState<string | null>(options[0]);
    const [checkRadio, setCheckRadio] = React.useState('หมดอายุ');
    const displayDate = checkRadio === 'หมดอายุ' ? true : false
    const [display, setDisplay] = React.useState(false);
    const dispatch = useDispatch()
    const {stepValues} = useSelector(formSelector)


    const formikStep1 = useFormik({
        initialValues: {
            product: '',
            price: '',
            productType: productType,
            listExpire: 'หมดอายุ',
            expireDate: '',

        },
        onSubmit: (values) => {

            setDisplay(true)
            dispatch(setStepValue({values}))

        },
        validationSchema: validatePD,

    })

    React.useEffect(() => {


    }, [])

    return (
        <>

            <form className={'form'} onSubmit={formikStep1.handleSubmit}>
                <Grid container spacing={2} columns={2} alignItems="center">
                    {!display && (
                        <>
                            <Grid item xs={5} md={4}>
                                <TextField id="standard-basic" label="ชื่อสินค้า"
                                           onChange={formikStep1.handleChange}
                                           name={'product'} value={formikStep1.values.product}
                                           error={formikStep1.errors.product ? true : false}
                                />
                                <Typography
                                    style={{fontSize: '12px'}}
                                    color={'error'}
                                >
                                    {formikStep1.errors.product}
                                </Typography>

                            </Grid>
                            <Grid item xs={12} md={4}>
                                <TextField id="standard-basic" label="ราคา"
                                           onChange={formikStep1.handleChange}
                                           name={'price'}
                                           value={formikStep1.values.price}
                                           error={formikStep1.errors.price ? true : false}
                                />
                                <Typography
                                    style={{fontSize: '12px'}}
                                    color={'error'}
                                >
                                    {formikStep1.errors.price}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={4}>
                                <Autocomplete
                                    value={productType}
                                    onChange={(event: any, newValue: string | null) => {
                                        setProductType(newValue);
                                        formikStep1.setValues({
                                            ...formikStep1.values,
                                            productType: newValue || ''
                                        })
                                    }}
                                    id="controllable-states-demo"
                                    options={options}
                                    sx={{width: 300}}
                                    renderInput={(params) => <TextField {...params} label="ประเภทสินค้า"/>}
                                />
                            </Grid>
                            <Grid item xs={12} md={4}>
                                <FormLabel id="demo-controlled-radio-buttons-group">เลือกรายการ</FormLabel>
                                <RadioGroup
                                    aria-labelledby="demo-controlled-radio-buttons-group"
                                    name="controlled-radio-buttons-group"
                                    value={checkRadio}
                                    onChange={(event, value) => {
                                        formikStep1.setValues({
                                            ...formikStep1.values,
                                            listExpire: value,
                                        })
                                        setCheckRadio(value)
                                    }}

                                >
                                    <FormControlLabel value="หมดอายุ" control={<Radio/>} label="หมดอายุ"/>
                                    <FormControlLabel value="ไม่หมดอายุ" control={<Radio/>} label="ไม่หมดอายุ"/>
                                </RadioGroup>
                                {displayDate && (
                                    <>
                                        <TextField
                                            name={'expireDate'}
                                            value={formikStep1?.values?.expireDate}
                                            label="วันที่หมดอายุ"
                                            InputLabelProps={{shrink: true, required: true}}
                                            type="date"
                                            defaultValue={materialDateInput}
                                            onChange={formikStep1?.handleChange}
                                            error={formikStep1?.errors?.expireDate ? true : false}
                                        />
                                        <Typography
                                            style={{fontSize: '12px'}}
                                            color={'error'}
                                        >
                                            {formikStep1.errors.expireDate}
                                        </Typography>
                                    </>
                                )}
                            </Grid>
                            <Grid item xs={12} md={4}>
                                <div>
                                    <Button variant="contained" type={"submit"}>
                                        บันทึก
                                    </Button>
                                </div>
                            </Grid>
                        </>
                    )}
                </Grid>
                {display && (

                    <>
                        <Box component="span" className={'detail-product'}
                             sx={{
                                 p: 2, border: '1px dashed grey',
                                 width: 500,
                                 background: "#CCFF99",

                             }}>
                            <h1>รายละเอียดสินค้า</h1>
                            <p>ชื่อสินค้า : {stepValues?.product} </p>
                            <p>ราคา : {stepValues?.price} </p>
                            <p>ประเภท : {stepValues?.productType} </p>
                            <p>วันหมดอายุ : {stepValues?.expireDate?stepValues?.expireDate:'-'} </p>
                        </Box>
                        <Grid container spacing={2} columns={1} alignItems="center" className={'back'}>
                            <Button variant="contained" onClick={() => setDisplay(false)}>
                                Back
                            </Button>
                        </Grid>
                    </>
                )}
            </form>
        </>
    )
}