import './App.css';
import { BrowserRouter as Router,Routes,Route } from 'react-router-dom';
import Product from "./components/form/ProductForm";
import {store} from "./redux/store";
import { Provider} from "react-redux";
import Header from "./components/Header";
import Pokemon from "./components/form/Pokemon";
import Home from "./components/form/Home";



function App() {
    return (
        <>
            <Provider store={store}>
            <Router>
                <nav>
                  <Header />
                </nav>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/product" element={<Product/>}/>
                    <Route path="/apipoke" element={<Pokemon/>} />
                </Routes>
            </Router>
            </Provider>

        </>
    );
}

export default App;